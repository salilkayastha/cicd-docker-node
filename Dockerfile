FROM node:14.17.3-alpine3.14

WORKDIR /app

COPY package*.json ./

RUN apt update && apt install -y npm && m && npm install
RUN npm install -y -g webpack-cli && npm install

RUN npm ci

COPY . .

EXPOSE 3000

CMD ["npm", "start"]
